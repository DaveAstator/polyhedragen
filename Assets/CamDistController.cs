﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;
using UnityEngine.UI;

public class CamDistController : MonoBehaviour
{
    // Start is called before the first frame update
    private Vector3 dir;
    void Start()
    {
        dir = (Camera.main.transform.position- Camera.main.GetComponent<LookAtConstraint>().GetSource(0).sourceTransform.position).normalized;
    }
    public void Change(float val)
    {
        Camera.main.transform.position = Camera.main.GetComponent<LookAtConstraint>().GetSource(0).sourceTransform.position + (dir * val);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.mouseScrollDelta.y!=0)
        {
            GetComponent<Slider>().value += Input.mouseScrollDelta.y*3;
            
        }
    }
}
