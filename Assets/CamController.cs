﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CamController : MonoBehaviour
{
    bool isDragging = false;
    Vector3 origin;
    public GameObject rotationTarget;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                isDragging = true;
                origin = Input.mousePosition;
            }
        }
        if (isDragging)
        {
            var ax = (Input.mousePosition - origin)/50f;
            rotationTarget.transform.RotateAround(Camera.main.transform.up, -ax.x);
            rotationTarget.transform.RotateAround(Camera.main.transform.right, ax.y);
            origin = Input.mousePosition;
            //rotationTarget.transform.Rotate(new Vector3(ax.x, ax.y, 0));

        }
        if (Input.GetMouseButtonUp(0))
        {
            isDragging = false;
        }
    }
}
