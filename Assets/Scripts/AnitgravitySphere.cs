﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public enum GenerationTypes
{
    VertsAsVerts,
    VertsAsFaceCenters,
    PairwiseMidpointsAsVerts
}

public class AnitgravitySphere : MonoBehaviour
{

    public float force = 10f;
    public float maxrad = 5f;
    [Range(0, 10)]
    public float customMaxLen = 1f;

    [Range(0, 10)]
    public float distanceFromFaceCenter = 1f;
    [Range(0, 1200)]
    public float distanceFromCenter = 1f;
    [Range(0, 1200)]
    public float minDistanceFromCenter = 0f;

    public GameObject meshObj;
    private List<GameObject> planes = new List<GameObject>();
    public bool vertsAsFaces = true;

    public GenerationTypes genType=0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        UpdateSpheres();
        switch (genType)
        {
            case (GenerationTypes.VertsAsVerts):
                UpdateMesh();
                break;
            case GenerationTypes.VertsAsFaceCenters:
                GetComponent<PlanarSolver>().GenerateFacesOnPlanarIntersections(distanceFromCenter, minDistanceFromCenter);
                break;
            case GenerationTypes.PairwiseMidpointsAsVerts:
                UpdateMeshMidEdgeMethod();
                break;
            default:
                break;
        }
    }
    public void ChangeGenType(int val)
    {
        genType = (GenerationTypes)val;
    }

    private void UpdateSpheres()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            for (int k = i + 1; k < transform.childCount; k++)
            {
                Distract(transform.GetChild(i), transform.GetChild(k));
            }
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            if (gameObject != transform.GetChild(i).gameObject)
            {
                ClampPositionProject(transform.GetChild(i));
            }
        }
    }

    private void Distract(Transform obj1, Transform obj2)
    {

        float dist = Vector3.Distance(obj1.transform.position, obj2.transform.position);
        float distReciporal = 1 / (dist * dist);
        float resforce = force * distReciporal;
        Vector3 obj1MoveDir = (obj1.transform.position - obj2.transform.position).normalized;
        Vector3 obj2MoveDir = (obj2.transform.position - obj1.transform.position).normalized;
        obj1.GetComponent<Rigidbody>().velocity += obj1MoveDir * resforce;
        obj2.GetComponent<Rigidbody>().velocity += obj2MoveDir * resforce;

    }

    private void ClampPositionProject(Transform child)
    {
        //float maxrad = 5f;
        if (Vector3.Distance(child.transform.position, transform.position) > maxrad)
        {
            // print($"Out of radius{ transform.position}");
            child.transform.position += child.GetComponent<Rigidbody>().velocity;

            Vector3 dirvec = (child.transform.position - transform.position).normalized;
            Vector3 respos = transform.position + dirvec.normalized * (maxrad);
            child.transform.position = respos;
            child.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
    }

    private void ClampPosition(Transform child)
    {
        //float maxrad = 5f;
        if (Vector3.Distance(child.position, transform.position) > maxrad)
        {
            // print($"Out of radius{ transform.position}");
            Vector3 dirvec = (child.position - transform.position).normalized;
            Vector3 respos = transform.position + dirvec.normalized * (maxrad);
            child.position = respos;

            //child.GetComponent<Rigidbody>().velocity = -dirvec;
            child.GetComponent<Rigidbody>().velocity = child.GetComponent<Rigidbody>().velocity / 2f;
        }
    }
    private void ClampPositionSoft(Transform child)
    {
        //float maxrad = 5f;
        if (Vector3.Distance(child.position, transform.position) > maxrad)
        {
            // print($"Out of radius{ transform.position}");
            Vector3 dirvec = (child.position - transform.position).normalized;
            child.GetComponent<Rigidbody>().velocity = dirvec * 3;
        }
    }




    private void UpdateMeshFaceMethod()
    {

        var sArea = maxrad * maxrad * 4 * 3.1416f;
        var segs = transform.childCount;
        var maxlen = Mathf.Sqrt((sArea / segs) / 3.1416f);

        var midVertices = new List<Vector3>();
        //generate vertices on edge midpoints
        print(transform.childCount);
        for (int a = 2; a < transform.childCount; a++)
            for (int b = 1; b < a; b++)
                for (int c = 0; c < b; c++)
                {

                    Vector3 newVert = ((transform.GetChild(a).transform.position +
                        transform.GetChild(b).transform.position +
                        transform.GetChild(c).transform.position) / 3f);


                    if ((Vector3.Distance(newVert, transform.position) > maxrad * 0.5f))
                    {
                        midVertices.Add(newVert);
                    }

                }

        GenerateSurfaceOnVertices(maxlen, midVertices);
    }

    private void GenerateSurfaceOnVertices(float maxlen, List<Vector3> customVertList)
    {
        List<Vector3> vertList = new List<Vector3>();
        List<Vector3> normList = new List<Vector3>();
        List<int> trisList = new List<int>();

        for (int a = 0; a < customVertList.Count; a++)
        {
            var normal = (customVertList[a] - transform.position);
            normList.Add(normal);
        }

        for (int a = 2; a < customVertList.Count; a++)
            for (int b = 1; b < a; b++)
                for (int c = 0; c < b; c++)
                {
                    Vector3 posa, posb, posc;
                    posa = (customVertList[a]);
                    posb = (customVertList[b]);
                    posc = (customVertList[c]);
                    var d1 = Vector3.Distance(posa, posb);
                    var d2 = Vector3.Distance(posb, posc);
                    var d3 = Vector3.Distance(posa, posc);

                    if (((d1 + d2 + d3) / 3f) < maxlen)
                    {
                        var surfaceNormal = Vector3.Cross(posb - posa, posc - posa);
                        var avgpos = (posa + posb + posc) / 3f;
                        var outDir = avgpos - transform.position;

                        if (Vector3.Dot(surfaceNormal, outDir) > 0)
                        {
                            trisList.Add(a);
                            trisList.Add(b);
                            trisList.Add(c);
                        }
                        else
                        {
                            trisList.Add(c);
                            trisList.Add(b);
                            trisList.Add(a);
                        }
                    }
                }

        var mesh = meshObj.GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = customVertList.ToArray();
        mesh.triangles = trisList.ToArray();
        mesh.normals = normList.ToArray();
        // mesh.RecalculateTangents();
        //mesh.RecalculateNormals();
        print(mesh.normals.Length);
        meshObj.GetComponent<MeshFilter>().mesh = mesh;
    }

    private void UpdateMeshMidEdgeMethod()
    {
        Vector3[] verts = new Vector3[0];
        int[] triangles = new int[0];

        List<Vector3> vertList = new List<Vector3>();
        List<Vector3> normList = new List<Vector3>();
        List<int> trisList = new List<int>();
        var sArea = maxrad * maxrad * 4 * 3.1416f;
        var segs = transform.childCount;
        var maxlen = Mathf.Sqrt((sArea / segs) / 3.1416f);

        var midVertices = new List<Vector3>();
        //generate vertices on edge midpoints
        for (int a = 1; a < transform.childCount; a++)
            for (int b = 0; b < a; b++)
            {
                if(Vector3.Distance(transform.GetChild(a).transform.position, transform.GetChild(b).transform.position)<= maxlen*3)
                midVertices.Add((transform.GetChild(a).transform.position + transform.GetChild(b).transform.position) / 2f);

            }

        for (int a = 0; a < midVertices.Count; a++)
        {
            var normal = (midVertices[a] - transform.position);
            normList.Add(normal);
        }


        for (int a = 2; a < midVertices.Count; a++)
        {
            for (int b = 1; b < a; b++)
            {
                for (int c = 0; c < b; c++)
                {
                    Vector3 posa, posb, posc;
                    posa = (midVertices[a]);
                    posb = (midVertices[b]);
                    posc = (midVertices[c]);
                    var d1 = Vector3.Distance(posa, posb);
                    var d2 = Vector3.Distance(posb, posc);
                    var d3 = Vector3.Distance(posa, posc);


                    if (((d1 + d2 + d3) / 3f) < maxlen)
                    {
                        var surfaceNormal = Vector3.Cross(posb - posa, posc - posa);
                        var avgpos = (posa + posb + posc) / 3f;
                        var outDir = avgpos - transform.position;

                        if (Vector3.Dot(surfaceNormal, outDir) > 0)
                        {
                            trisList.Add(a);
                            trisList.Add(b);
                            trisList.Add(c);
                        }
                        else
                        {
                            trisList.Add(c);
                            trisList.Add(b);
                            trisList.Add(a);
                        }



                    }

                }

            }

        }

        var mesh = meshObj.GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = midVertices.ToArray();
        mesh.triangles = trisList.ToArray();
        mesh.normals = normList.ToArray();
        // mesh.RecalculateTangents();
        //mesh.RecalculateNormals();
        print(mesh.normals.Length);
        meshObj.GetComponent<MeshFilter>().mesh = mesh;
    }
    private void UpdateMesh()
    {
        Vector3[] verts = new Vector3[0];
        int[] triangles = new int[0];

        List<Vector3> vertList = new List<Vector3>();
        List<Vector3> normList = new List<Vector3>();
        List<int> trisList = new List<int>();
        var sArea = maxrad * maxrad * 4 * 3.1416f;
        var segs = transform.childCount;
        var maxlen = Mathf.Sqrt((sArea / segs) / 3.1416f);


        for (int a = 0; a < transform.childCount; a++)
        {
            vertList.Add(transform.GetChild(a).transform.position);
            var normal = (transform.GetChild(a).transform.position - transform.position);
            normList.Add(normal);
        }

        for (int a = 2; a < transform.childCount; a++)
        {
            if (gameObject != transform.GetChild(a).gameObject)
            {
                for (int b = 1; b < a; b++)
                {
                    if (gameObject != transform.GetChild(b).gameObject)
                    {
                        for (int c = 0; c < b; c++)
                        {
                            if (gameObject != transform.GetChild(c).gameObject)
                            {
                                var posa = transform.GetChild(a).transform.position;
                                var posb = transform.GetChild(b).transform.position;
                                var posc = transform.GetChild(c).transform.position;
                                var d1 = Vector3.Distance(posa, posb);
                                var d2 = Vector3.Distance(posb, posc);
                                var d3 = Vector3.Distance(posa, posc);


                                if (((d1 + d2 + d3) / 3f) < maxlen * 2)
                                {
                                    var surfaceNormal = Vector3.Cross(vertList[b] - vertList[a], vertList[c] - vertList[a]);
                                    var avgpos = (vertList[a] + vertList[b] + vertList[c]) / 3f;
                                    var outDir = avgpos - transform.position;
                                    //define triangle orientation
                                    if (Vector3.Dot(surfaceNormal, outDir) > 0)
                                    {
                                        trisList.Add(a);
                                        trisList.Add(b);
                                        trisList.Add(c);
                                    }
                                    else
                                    {
                                        trisList.Add(c);
                                        trisList.Add(b);
                                        trisList.Add(a);
                                    }


                                }
                            }
                        }
                    }
                }
            }
        }
        GetComponent<MeshGenerator>().GenerateFromLists(vertList, trisList, normList);
        /*
                var mesh = meshObj.GetComponent<MeshFilter>().mesh;
                mesh.Clear();
                mesh.vertices = vertList.ToArray();
                mesh.triangles = trisList.ToArray();
                mesh.normals = normList.ToArray();
                // mesh.RecalculateTangents();
                //mesh.RecalculateNormals();
                print(mesh.normals.Length);
                meshObj.GetComponent<MeshFilter>().mesh = mesh;*/

    }
    public void Add()
    {
        var a = Instantiate(transform.GetChild(0), transform);
        a.GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
    public void Del()
    {
        if (transform.childCount > 1)
        {
            Destroy(transform.GetChild(transform.childCount - 1).gameObject);
        }
    }
}
