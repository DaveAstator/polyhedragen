﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolyFace
{
   public  List<Vector3> points=new List<Vector3>();

    public PolyFace()
    {
        points = new List<Vector3>();
    }
}
public class TriMesh
{
    public List<Vector3> vertices = new List<Vector3>();
    public List<int> tris = new List<int>();

    public void AddTriMesh(TriMesh addMesh)
    {
        var lastVert = vertices.Count;

        vertices.AddRange(addMesh.vertices);
        foreach (var tri in addMesh.tris)
        {
            tris.Add(tri + lastVert );
        }
    }
    public void AddTriangle(Vector3 a, Vector3 b, Vector3 c)
    {
        vertices.Add(a);
        vertices.Add(b);
        vertices.Add(c);
        tris.Add(vertices.Count - 1);
        tris.Add(vertices.Count - 2);
        tris.Add(vertices.Count - 3);
    }
}

public class PolyFaceGen : MonoBehaviour
{
    // Start is called before the first frame update
    public TriMesh TriangulatePolyFace(PolyFace pf, Vector3? axis = null)
    {
        return TriangulatePolyFace(pf.points,axis);
    }
    public TriMesh TriangulatePolyFace(List<Vector3> points, Vector3? axis =null)
    {
        List<Vector3> chainedPoints = OrderPoints(points, axis);
        
        TriMesh mesh = new TriMesh();
        if (chainedPoints.Count < 3)
            return mesh;
        /*
        mesh.AddTriangle(chainedPoints[0], chainedPoints[1], chainedPoints[2]);
        for (int i = 3; i < chainedPoints.Count; i++)
        {
            mesh.AddTriangle(chainedPoints[0], chainedPoints[i-1], chainedPoints[i]);
        }*/

        var center = AvgPoint(chainedPoints);

        for (int i = 1; i < chainedPoints.Count; i++)
        {
            mesh.AddTriangle(chainedPoints[i - 1], center, chainedPoints[i]);
        }
        mesh.AddTriangle(chainedPoints[chainedPoints.Count - 1], center, chainedPoints[0]);
        return mesh;
    }

    private void Update()
    {
        //GenFacesFromChildren();
    }
    public void GenFacesFromChildren()
    {
        List<Vector3> points = new List<Vector3>();
        for (int a = 0; a < transform.childCount; a++)
        {
            points.Add(transform.GetChild(a).position);
        }
        var mesh = TriangulatePolyFace(points);
        GetComponent<MeshGenerator>().GenerateFromTriMesh(mesh);
    }

    public Vector3 AvgPoint(List<Vector3> points)
    {
        var sumpt = new Vector3();

        foreach (var pt in points)
        {
            sumpt = sumpt + pt;
        }
        var res = (sumpt / (float)points.Count);
        return res;

    }

    private List<Vector3> OrderPoints(List<Vector3> points, Vector3? axis)
    {
        if (points.Count < 3) 
            return new List<Vector3>();
        var chainedPoints = new List<Vector3>();
        var center = AvgPoint(points);
        chainedPoints.Add(points[0]);
        points.RemoveAt(0);

        var upward = new Vector3();
        if (!axis.HasValue)
        {
            upward = Vector3.Cross(chainedPoints[0] - center, points[0] - center);
        }
        else
        {
            upward = axis.Value;
        }
        int k = 0;
        while (points.Count > 0)
        {
            var minDist = 900000f;
            var minAngle = 90000f;
            int minID = 0;

            for (int i = 0; i < points.Count; i++)
            {

                var angle = Vector3.SignedAngle(chainedPoints[chainedPoints.Count - 1] - center, points[i] - center,upward);
                var dist = Vector3.Distance(chainedPoints[chainedPoints.Count - 1], points[i]);
               // print($"{angle},{k}");

                 if ((angle < minAngle) && angle>0)
                    {
                            minAngle = angle;
                            //minDist = dist;
                            minID = i;
                    }
                k += 1;
            }


            chainedPoints.Add(points[minID]);
            points.RemoveAt(minID);
        }

        return chainedPoints;
    }
}
