﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Single;
public class PlanarSolver : MonoBehaviour
{

    class PlaneEQ
    {

        // An equation of the plane containing the point(x0, y0, z0) with normal vector N~ = <A, B, C>
        //is
        //A(x − x0) + B(y − y0) + C(z − z0) = 0.
        //plane equation is Ax+By+Cz+D=0;

        public float A, B, C, D;

        public PlaneEQ(Vector3 normal, Vector3 point)
        {
            A = normal.x;
            B = normal.y;
            C = normal.z;
            D = A * (-point.x) + B * (-point.y) + C * (-point.z);
        }

    }

    class PlaneIntersection
    {
        PlaneEQ p1, p2, p3;

        public PlaneIntersection(PlaneEQ p1, PlaneEQ p2, PlaneEQ p3)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.p3 = p3;
        }

        public Vector3? GetIntersect()
        {

            Matrix<float> Rc = DenseMatrix.OfArray(new float[,]{
            { p1.A, p1.B, p1.C},
            { p2.A, p2.B, p2.C},
            { p3.A, p3.B, p3.C}});

            Matrix<float> Rd = DenseMatrix.OfArray(new float[,]{
            { p1.A, p1.B, p1.C, p1.D},
            { p2.A, p2.B, p2.C, p2.D},
            { p3.A, p3.B, p3.C, p3.D}});

            var det = Rc.Determinant();
            bool isInRank = (Rc.Rank() == 3 && Rd.Rank() == 3);


            Vector3 intersectionPoint = Vector3.zero;
            if (det != 0 && isInRank)
            {
                Matrix<float> Mx = DenseMatrix.OfArray(new float[,]{
            { p1.D, p1.B, p1.C, },
            { p2.D, p2.B, p2.C, },
            { p3.D, p3.B, p3.C, }});

                Matrix<float> My = DenseMatrix.OfArray(new float[,]{
            {p1.A, p1.D, p1.C, },
            {p2.A, p2.D, p2.C, },
            {p3.A, p3.D, p3.C, }});

                Matrix<float> Mz = DenseMatrix.OfArray(new float[,]{
            { p1.A, p1.B, p1.D},
            { p2.A, p2.B, p2.D},
            { p3.A, p3.B, p3.D}});

                intersectionPoint.x = Mx.Determinant() / -det;
                intersectionPoint.y = My.Determinant() / -det;
                intersectionPoint.z = Mz.Determinant() / -det;

                return intersectionPoint;
            }
            else return null;

        }

    }

    public void GenerateFacesOnPlanarIntersections(float maxDistFromCenter, float minDistFromCenter = 0)
    {
        List<Vector3> vertList = new List<Vector3>();
        List<Vector3> normList = new List<Vector3>();
        List<int> trisList = new List<int>();
        List<PolyFace> polyFaces = new List<PolyFace>();
        for (int i = 0; i < transform.childCount; i++)
        {
            polyFaces.Add(new PolyFace());
        }

        var intersectionVectices = new List<Vector3>();
        var planarVertices = new List<Vector3>();
        for (int a = 2; a < transform.childCount; a++)
            for (int b = 1; b < a; b++)
                for (int c = 0; c < b; c++)
                {
                    PlaneEQ p1 = new PlaneEQ(transform.GetChild(a).transform.position - transform.position, transform.GetChild(a).transform.position);
                    PlaneEQ p2 = new PlaneEQ(transform.GetChild(b).transform.position - transform.position, transform.GetChild(b).transform.position);
                    PlaneEQ p3 = new PlaneEQ(transform.GetChild(c).transform.position - transform.position, transform.GetChild(c).transform.position);

                    //point where three planes intersect
                    Vector3? intersectPoint = new PlaneIntersection(p1, p2, p3).GetIntersect();
                    if (intersectPoint.HasValue)
                    {
                        //if (Vector3.Distance(intersectPoint.Value, transform.GetChild(a).position) < distanceFromFaceCenter)
                        var dst = Vector3.Distance(intersectPoint.Value, transform.position);
                        if ( dst <= maxDistFromCenter && dst>minDistFromCenter)
                        {
                            intersectionVectices.Add(intersectPoint.Value);
                            polyFaces[a].points.Add(intersectPoint.Value);
                            polyFaces[b].points.Add(intersectPoint.Value);
                            polyFaces[c].points.Add(intersectPoint.Value);
                        }

                    }
                }


        TriMesh newmesh = new TriMesh();
        for (int i = 0; i < polyFaces.Count; i++)
        {
            var axis = transform.GetChild(i).transform.position - transform.position;
            newmesh.AddTriMesh(GetComponent<PolyFaceGen>().TriangulatePolyFace(polyFaces[i], axis));
        }
        GetComponent<MeshGenerator>().GenerateFromTriMesh(newmesh);
        /*
        var sArea = maxrad * maxrad * 4 * 3.1416f;
        var segs = transform.childCount;
        var maxlen = Mathf.Sqrt((sArea / segs) / 3.1416f) * 4;
        GenerateSurfaceOnVertices(customMaxLen, intersectionVectices);
        */

        //for each vertex create big poly perpendicular to surface.
        // brute force find all intersection points with all other polys. and create real polys of them.
    }
}
