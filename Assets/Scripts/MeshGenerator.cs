﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshGenerator : MonoBehaviour
{
    public GameObject meshTarget;

    // Start is called before the first frame update
    public void GenerateFromTriMesh(TriMesh triMesh)
    {
        GenerateFromLists(triMesh.vertices, triMesh.tris);
    }
    public void GenerateFromLists(List<Vector3> vertices, List<int> triangles)
    {
        var mesh = meshTarget.GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.RecalculateNormals();
        //print(mesh.normals.Length);
       // GetComponent<MeshFilter>().mesh = mesh;
    }
    public void GenerateFromLists(List<Vector3> vertices, List<int> triangles, List<Vector3> normals)
    {
        var mesh = meshTarget.GetComponent<MeshFilter>().mesh;
        mesh.Clear();
        mesh.vertices = vertices.ToArray();
        mesh.triangles = triangles.ToArray();
        mesh.normals = normals.ToArray();
        //mesh.RecalculateNormals();
        //print(mesh.normals.Length);
        //GetComponent<MeshFilter>().mesh = mesh;
    }
}
